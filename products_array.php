<?php 
	
	$products = [
	['title'=>'Ноутбук Dell Inspiron',
	 'price_type'=>'Гривна',
	 'price_val'=> 100,
	 'discount_type'=> 'гривень',
	 'discount_val'=>20,
	],
    ['title'=>'Ноутбук Asus Vivobook',
	 'price_type'=>'Евро',
	 'price_val'=> 1000,
	 'discount_type' => "%",
	 'discount_val'=>20
	],
	['title'=>'Ноутбук HP 250 G5 ',     
	 'price_type'=>'Доллар',
	 'price_val'=> 500,
	 'discount_type' => "%",
	 'discount_val'=>35
	],
	['title'=>'Ноутбук Acer Aspire',
	 'price_type'=>'Евро',
	 'price_val'=> 600,
	 'discount_type' => '%',
	 'discount_val'=>50,
	],
	['title'=>'Ноутбук Lenovo IdeaPad',
	 'price_type'=>'Гривна',
	 'price_val'=> 18000,
	 'discount_type' => '%',
	 'discount_val'=>70
	],
	['title'=>'Ноутбук Apple A1466',
	 'price_type'=>'Гривна',
	 'price_val'=> 30000,
	 'discount_type' => 'гривень',
	 'discount_val'=>5000
	],
	['title'=>'Ноутбук Prestigio Smartbook 141A03',
	 'price_type'=>'Гривна',
	 'price_val'=> 8000,
	 'discount_type' => 'гривень',
	 'discount_val'=>1200
	],
	['title'=>'Ноутбук MSI GE72MVR-7RG',
	 'price_type'=>'Гривна',
	 'price_val'=> 10000,
	 'discount_type' => 'гривень',
	 'discount_val'=>2000
	],
	['title'=>'Ноутбук Xiaomi Mi Notebook Air 12 M3',
	 'price_type'=>'Доллар',
	 'price_val'=> 500,
	 'discount_type' => 'гривень',
	 'discount_val'=>100
	],
	['title'=>'Ноутбук Razer Stealth',
	 'price_type'=>'Евро',
	 'price_val'=> 400,
	 'discount_type' => '%',
	 'discount_val'=>5
	]
];
?>